/* frameworks */
//=include ../../node_modules/jquery/dist/jquery.min.js

/* libs */
//=include lib/modernizr-custom.js

/* plugins */
//=include ../../node_modules/popper.js/dist/umd/popper.min.js
//=include ../../node_modules/bootstrap/js/dist/util.js
//=include ../../node_modules/bootstrap/js/dist/dropdown.js
//=include ../../node_modules/bootstrap/js/dist/collapse.js
//=include ../../node_modules/bootstrap/js/dist/tab.js

/* separate */
//=include helpers/object-fit.js
//=include helpers/valid.js
//=include separate/global.js
//=include helpers/bs_modal_fix.js
//=include helpers/bsModalCenter.js
//=include helpers/modal-scroll-fix.js
//=include plugins/bs/modal.js